package ini

// Encode an interface (typically a struct) into INI-formatted data
// Known types:
// - Any kind of ints
// - Any kind of unsigned ints
// - Structs + pointers
// - Boolean
// - Strings
// -  Types with the method `MarshalText`
//
// Unknown types (also known types!) can impement the method MarshalINI
// to be to override standard marshaling method, or make it marshalable if it's not.
//
// e.g: see types.go and the Time example. Time does implement MarshalText, but it doesnt
// store the time-value in a very readable/editable manner, so the ini.Time is just a
// wrapper-struct with the Marshaler-interface to read/write the time-value differently.

import (
	"bytes"
	"reflect"
)

// Marshaler is an interface to implement for unknown types
type Marshaler interface {
	MarshalINI() ([]byte, error)
}

type encoder struct {
	bytes.Buffer

	/*cache sync.Map TODO */
}

// Marshal encode interface v into INI-format
func Marshal(v interface{}) ([]byte, error) {

	enc := &encoder{}

	if err := enc.marshal(v); err != nil {
		return nil, err
	}

	return enc.Bytes(), nil
}

// marshal maps struct to ini
func (e *encoder) marshal(v interface{}) error {
	return e.imap("", reflect.ValueOf(v), nil, 0)
}

// write to byte buffer whenever a successfull encoding occurs
func (e *encoder) write(ns string, f *field, fn encodeFn) error {
	f.fn = fn
	b, err := f.encode()
	if b != nil && err == nil {
		if _, err := e.Write(append(b, '\n')); err != nil {
			return err
		}
	}
	return err
}

// imap does the actual mapping on a specific namespace
func (e *encoder) imap(ns string, v reflect.Value, p interface{}, i int) error {
	f := newField(v, p, i)

	f.setNS(ns)

	if f.implements(marshalerType) || f.implements(textMarshalerType) {
		return e.write(ns, f, f.marshalerDecoder)
	}

	switch v.Kind() {
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		return e.write(ns, f, f.intEncoder)
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
		return e.write(ns, f, f.uintEncoder)
	case reflect.Float32:
		return e.write(ns, f, f.float32Encoder)
	case reflect.Float64:
		return e.write(ns, f, f.float64Encoder)
	case reflect.String:
		return e.write(ns, f, f.strEncoder)
	case reflect.Bool:
		return e.write(ns, f, f.boolEncoder)
	case reflect.Struct:

		if err := e.write(f.ns, f, f.structEncoder); err != nil {
			return err
		}

		for i := 0; i < f.v.NumField(); i++ {
			if err := e.imap(f.ns, f.v.Field(i), f.t, i); err != nil {
				return err
			}
		}
	case reflect.Ptr:
		if v.IsNil() {
			return nil
		}
		return e.imap(ns, v.Elem(), p, i)
	}

	return nil
}
